import {useEffect, useState} from 'react'
import styles from './Home.module.css'

const items = {
    pizzaMexicana: {name: 'Pizza mexicana', cost: 7, isBeverage: false},
    pizzaRucolaXXL: {name: 'Pizza Rucola XXL', cost: 14, isBeverage: false},
    pizzaChicken: {name: 'Pizza Chicken', cost: 8.5, isBeverage: false},
    coke: {name: 'Coke', cost: 3, isBeverage: true},
    beer: {name: 'Beer', cost: 2, isBeverage: true},
}

const minTotal = 10
const minForFreeBeverage = 30

function Item(props) {
    const {name, id, cost, ammount, onAdd, onRemove, freeBev} = props
    return (
        <div className={styles.item}>
            <div className={styles.itemName}>{name}</div>
            <div className={styles.itemProp}>&euro;{cost}</div>
            <div className={styles.itemProp}>
                <div
                    className={`${styles.circleStuff} ${styles.add}`}
                    onClick={() => {
                        onAdd(id)
                    }}
                >
                    +
                </div>
            </div>
            <div className={styles.itemProp}>{ammount}</div>
            <div className={styles.itemProp}>
                {ammount > 0 && (
                    <div
                        className={`${styles.circleStuff} ${styles.remove}`}
                        onClick={() => {
                            onRemove(id)
                        }}
                    >
                        ×
                    </div>
                )}
            </div>
        </div>
    )
}

export default function Home(props) {
    const [cart, setCart] = useState({})
    const [freeBev, setFreeBev] = useState(null)
    useEffect(() => {
        document.title = "PizzaShop by 6. csoport"
    })

    const total = Object.keys(cart).reduce((acc, key) => {
        acc += items[key].cost * cart[key].amm
        return acc
    }, 0)

    const foodTotal = Object.keys(cart).reduce((acc, key) => {
        acc += items[key].isBeverage ? 0 : items[key].cost * cart[key].amm
        return acc
    }, 0)

    return (
        <div className={styles.container}>
            <div className={styles.title}>Shopping</div>
            <div className={styles.itemContainer}>
                {Object.keys(items).map((key, i) => {
                    const item = items[key]
                    return (
                        <Item
                            id={key}
                            freeBev={freeBev}
                            ammount={cart[key] ? cart[key].amm : 0}
                            onRemove={(id) => {
                                setCart({
                                    ...cart,
                                    [id]: {amm: 0},
                                })
                            }}
                            onAdd={(id) => {
                                if (
                                    foodTotal > minForFreeBeverage &&
                                    items[id].isBeverage &&
                                    !freeBev
                                ) {
                                    setFreeBev(items[id])
                                    return
                                }
                                if (!cart[id]) {
                                    setCart({
                                        ...cart,
                                        [id]: {amm: 1},
                                    })
                                } else {
                                    setCart({
                                        ...cart,
                                        [id]: {amm: cart[id].amm + 1},
                                    })
                                }
                            }}
                            key={key}
                            {...item}
                        />
                    )
                })}
            </div>
            <div>
                <div className={styles.total}>Total price: &euro;{total}</div>
                {total < minTotal && (
                    <div className={styles.orderMore}>
                        You should order more for at least {minTotal - total} euro(s)
                    </div>
                )}
                {foodTotal > minForFreeBeverage && (
                    <div className={styles.beverage}>
                        {'You can choose a free beverage'}
                        {freeBev ? (
                            <>
                                {':'}
                                <span className={styles.freeBevName}>{` ${freeBev.name}`}</span>
                            </>
                        ) : (
                            '!'
                        )}
                    </div>
                )}
            </div>
            <div className={styles.buttonContainer}>
                <div
                    className={styles.button}
                    onClick={() => {
                        setCart({})
                        setFreeBev(null)
                    }}
                >
                    Cancel
                </div>
                <button
                    className={styles.button}
                    onClick={() => {
                        console.log(cart, foodTotal > minForFreeBeverage ? freeBev : null)
                        alert("Összesen: €" + total);
                    }}
                    disabled={total < 10}
                >
                    Pay
                </button>
            </div>
        </div>
    )
}
